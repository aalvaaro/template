# Bootswatch 3 Rails Template

## Setup

- Run the rails new command with the -m option:
  `rails new myapp -m https://raw.githubusercontent.com/AAlvAAro/bootswatch3-rails-template/master/rails_app_template.rb`

- You will be asked to write one of Bootswatch's template names in order to add that styling to the app. You can find the options
  here https://bootswatch.com/3/

- Cd into the project and run the setup script:
  `./bin/setup`

- You're done! Run the app
  'rails s'
